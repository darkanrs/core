plugins {
    `java-library`
    `maven-publish`
    id("com.github.ben-manes.versions") version "0.52.0"
}

publishing {
    publications {
        create<MavenPublication>("mavenJava") {
            from(components["java"])
        }
    }
    repositories {
        maven {
            name = "GitLabRepository"
            url = uri("https://gitlab.com/api/v4/projects/42379000/packages/maven")
            credentials(HttpHeaderCredentials::class) {
                name = "Job-Token"
                value = System.getenv("CI_JOB_TOKEN")
            }
            authentication {
                create<HttpHeaderAuthentication>("header")
            }
        }
    }
}

java {
    toolchain {
        languageVersion.set(JavaLanguageVersion.of(23))
    }
    withSourcesJar()
}

repositories {
    mavenLocal()
    mavenCentral()
}

group = "rs.darkan"
version = "2.0.2"

dependencies {
    implementation("com.google.code.gson:gson:2.12.1")
    implementation("com.google.guava:guava:33.4.0-jre")
    implementation("org.mongodb:mongodb-driver-sync:5.3.1")
    implementation("org.mongodb:mongodb-driver-core:5.3.1")
    implementation("io.netty:netty-all:4.1.117.Final")
    implementation("io.undertow:undertow-core:2.3.18.Final")
    implementation("io.github.classgraph:classgraph:4.8.179")
    implementation("com.squareup.okhttp3:okhttp:4.12.0")
}